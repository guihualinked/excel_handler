package org.xhliu.excel_handler;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.xhliu.excel_handler.controller.ExcelFileController;

import javax.annotation.Resource;

@SpringBootTest
class ExcelHandlerApplicationTests {

    @Resource
    private ExcelFileController fileController;

    @Test
    public void convertTest() {
        System.out.println(fileController.exampleFile());
    }
}
