package org.xhliu.excel_handler.entity;

import org.xhliu.excel_handler.annotations.CellProps;
import org.xhliu.excel_handler.annotations.RowProps;

/**
 * @author lxh
 */
@RowProps(rowStyle = "defaultXssStyle")
public class InsuredEntity {
    @CellProps(headerCellStyle = "defaultXssStyle",
            convert = "defaultCellTypeConvert",
            cellStyle = "defaultXssStyle",
            colName = "policy")
    private String policy;
    @CellProps(headerCellStyle = "defaultXssStyle",
            convert = "defaultCellTypeConvert",
            cellStyle = "defaultXssStyle",
            colName = "expiry")
    private String expiry;
    @CellProps(headerCellStyle = "defaultXssStyle",
            convert = "defaultCellTypeConvert",
            cellStyle = "defaultXssStyle",
            colName = "location")
    private String location;
    @CellProps(headerCellStyle = "defaultXssStyle",
            convert = "defaultCellTypeConvert",
            cellStyle = "defaultXssStyle",
            colName = "region")
    private String region;
    @CellProps(headerCellStyle = "defaultXssStyle",
            convert = "defaultCellTypeConvert",
            cellStyle = "defaultXssStyle",
            colName = "insuredValue")
    private String insuredValue;

    @CellProps(headerCellStyle = "defaultXssStyle",
            convert = "defaultCellTypeConvert",
            cellStyle = "defaultXssStyle",
            colName = "insuredValue")
    private String construction;

    @CellProps(headerCellStyle = "defaultXssStyle",
            convert = "defaultCellTypeConvert",
            cellStyle = "defaultXssStyle",
            colName = "businessType")
    private String businessType;

    @CellProps(headerCellStyle = "defaultXssStyle",
            convert = "defaultCellTypeConvert",
            cellStyle = "defaultXssStyle",
            colName = "earthquake")
    private String earthquake;

    @CellProps(headerCellStyle = "defaultXssStyle",
            convert = "defaultCellTypeConvert",
            cellStyle = "defaultXssStyle",
            colName = "flood")
    private String flood;

    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getInsuredValue() {
        return insuredValue;
    }

    public void setInsuredValue(String insuredValue) {
        this.insuredValue = insuredValue;
    }

    public String getConstruction() {
        return construction;
    }

    public void setConstruction(String construction) {
        this.construction = construction;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getEarthquake() {
        return earthquake;
    }

    public void setEarthquake(String earthquake) {
        this.earthquake = earthquake;
    }

    public String getFlood() {
        return flood;
    }

    public void setFlood(String flood) {
        this.flood = flood;
    }

    @Override
    public String toString() {
        return "InsuredEntity{" +
                "policy='" + policy + '\'' +
                ", expiry='" + expiry + '\'' +
                ", location='" + location + '\'' +
                ", region='" + region + '\'' +
                ", insuredValue='" + insuredValue + '\'' +
                ", construction='" + construction + '\'' +
                ", businessType='" + businessType + '\'' +
                ", earthquake='" + earthquake + '\'' +
                ", flood='" + flood + '\'' +
                '}';
    }
}
