package org.xhliu.excel_handler.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.xhliu.excel_handler.interfaces.impl.DefaultCellTypeConvert;

/**
 * @author lxh
 */
@Configuration
public class ExcelDataConvertConfig {

    @Bean(name = "defaultCellTypeConvert")
    public DefaultCellTypeConvert defaultCellTypeConvert() {
        return new DefaultCellTypeConvert();
    }
}
