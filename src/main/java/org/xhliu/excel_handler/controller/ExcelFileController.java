package org.xhliu.excel_handler.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.xhliu.excel_handler.entity.InsuredEntity;
import org.xhliu.excel_handler.interfaces.SheetData;
import org.xhliu.excel_handler.service.XssDataService;
import org.xhliu.excel_handler.tools.ExcelTool;

import java.util.List;

/**
 * @author lxh
 */
@RestController
@RequestMapping(path = "/excelFile")
public class ExcelFileController {

    private final ExcelTool excelTool;

    private final XssDataService dataService;

    @Autowired
    public ExcelFileController(ExcelTool excelTool,
                               XssDataService dataService) {
        this.excelTool = excelTool;
        this.dataService = dataService;
    }

    @GetMapping(path = "/example")
    public ResponseEntity<Resource> exampleFile() {
        SheetData<?> sheetData = (SheetData<InsuredEntity>) dataService::exampleData;
        InputStreamResource resource = new InputStreamResource(excelTool.singleSheet(sheetData));
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;fileName=" + sheetData.sheetName() + ".xlsx")
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(resource);
    }
}
