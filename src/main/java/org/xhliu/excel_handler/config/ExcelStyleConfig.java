package org.xhliu.excel_handler.config;

import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * @author lxh
 */
@Configuration
public class ExcelStyleConfig {
    @Bean(name = "defaultXssStyle")
    public XSSFCellStyle xssfCellStyle() {
        try (Workbook workbook = new SXSSFWorkbook()) {
            XSSFCellStyle style = (XSSFCellStyle) workbook.createCellStyle();
            CreationHelper helper = workbook.getCreationHelper();
            XSSFFont font = style.getFont();
            font.setFontName("宋体");
            font.setFamily(Font.ANSI_CHARSET);
            style.setFont(font);
            style.setDataFormat(helper.createDataFormat().getFormat("yyyy-MM-dd"));
            return style;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
