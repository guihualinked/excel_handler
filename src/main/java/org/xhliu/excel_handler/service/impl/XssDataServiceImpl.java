package org.xhliu.excel_handler.service.impl;

import org.apache.commons.codec.Resources;
import org.springframework.stereotype.Service;
import org.xhliu.excel_handler.entity.InsuredEntity;
import org.xhliu.excel_handler.service.XssDataService;
import org.xhliu.excel_handler.tools.ExcelTool;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author lxh
 */
@Service
public class XssDataServiceImpl
        implements XssDataService {

    @Resource
    private ExcelTool excelTool;

    private List<InsuredEntity> data = null;

    @Override
    public List<InsuredEntity> exampleData() {
        if (data != null) return data;
        try (InputStream in = Resources.getInputStream("data.xlsx")) {
            data = excelTool.read(InsuredEntity.class, in);
            return data;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
