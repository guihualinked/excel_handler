package org.xhliu.excel_handler.interfaces;

import com.google.common.reflect.TypeToken;

import java.util.List;

/**
 * Excel 中 Sheet 页的单个抽象
 *
 * @author lxh
 */
@FunctionalInterface
public interface SheetData<T> {
    /**
     * Sheet 页对应的数据
     */
    List<T> data();

    /**
     * Sheet 页数据的所属类型
     */
    default Class<?> dataClass() {
        List<T> data = data();
        if (data == null || data.isEmpty()) return Object.class;
        return data.get(0).getClass();
    }

    /**
     * 此 Sheet 的名称
     */
    default String sheetName() {
        return dataClass().getSimpleName();
    }
}
