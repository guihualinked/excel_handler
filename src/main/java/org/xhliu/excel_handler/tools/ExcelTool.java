package org.xhliu.excel_handler.tools;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.xhliu.excel_handler.annotations.CellProps;
import org.xhliu.excel_handler.annotations.RowProps;
import org.xhliu.excel_handler.interfaces.CellTypeConvert;
import org.xhliu.excel_handler.interfaces.SheetData;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author xhliu
 */
@Component
public class ExcelTool {

    private final static Logger log = LoggerFactory.getLogger(ExcelTool.class);

    private final static Map<Class<?>, List<Field>> cache = new ConcurrentHashMap<>();

    public static List<Field> getAllFields(Class<?> clazz) {
        return getAllFields(clazz, new ArrayList<>());
    }

    private static List<Field> getAllFields(Class<?> clazz, List<Field> list) {
        if (clazz == null || clazz == Object.class) return list;
        if (cache.containsKey(clazz)) {
            return cache.get(clazz);
        }
        Field[] fields = clazz.getDeclaredFields();
        list.addAll(Arrays.asList(fields));
        List<Field> allFields = getAllFields(clazz.getSuperclass(), list);
        cache.put(clazz, allFields);
        return allFields;
    }

    @Resource
    private ApplicationContext context;

    public <T> List<T> read(Class<T> clazz, InputStream in) {
        long start = System.currentTimeMillis();
        try (Workbook workbook = new XSSFWorkbook(in)) {
            Iterator<Sheet> iterator = workbook.sheetIterator();
            List<Field> fields = getAllFields(clazz);
            List<T> list = new ArrayList<>();
            while (iterator.hasNext()) {
                Sheet sheet = iterator.next();
                for (Row row : sheet) {
                    Constructor<?> constructor = clazz.getDeclaredConstructor();
                    @SuppressWarnings("unchecked")
                    T obj = (T) constructor.newInstance();
                    for (int i = 0; i < fields.size(); i++) {
                        Cell cell = row.getCell(i);
                        Field field = fields.get(i);
                        fillValue(field, cell, obj);
                    }
                    list.add(obj);
                }
            }
            long end = System.currentTimeMillis();
            log.info("Read Data Take Time: {} ms", end - start);
            return list;
        } catch (IOException | NoSuchMethodException | InstantiationException
                 | InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private void fillValue(Field field, Cell cell, Object target)
            throws IllegalAccessException {
        field.setAccessible(true);
        CellProps props = field.getAnnotation(CellProps.class);
        if (props == null) return;
        if (!props.convert().isEmpty()) {
            @SuppressWarnings("unchecked")
            CellTypeConvert<Cell, ?> convert = (CellTypeConvert<Cell, ?>) context.getBean(props.convert());
            if (convert.canConvert(cell.getCellType(), field.getType())) {
                field.set(target, convert.convert(cell));
            }
            return;
        }
        CellTypeConvert<Cell, ?> convert = context.getBean(props.convertClass());
        if (convert.canConvert(cell.getCellType(), field.getType())) {
            field.set(target, convert.convert(cell));
        }
    }

    /**
     * 将单个的 Sheet 数据转换为对应的 Excel 文件的比特流
     *
     * @param sheetData 单个 Sheet 页的数据对象
     * @return 经过 POI 处理的 Excel 比特流
     */
    public ByteArrayInputStream singleSheet(SheetData<?> sheetData) {
        long start = System.currentTimeMillis();
        try (
                Workbook workbook = new SXSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream()
        ) {
            genSheetData(sheetData, workbook);
            workbook.write(out);
            long end = System.currentTimeMillis();
            log.info("Gen Xlsx Take Time: {} ms", (end - start));
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 多个 Sheet 页的处理方法
     */
    public ByteArrayInputStream multipleSheet(List<SheetData<?>> sheetDataList) {
        try (
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream()
        ) {
            if (sheetDataList == null || sheetDataList.isEmpty()) {
                return new ByteArrayInputStream(out.toByteArray());
            }
            for (SheetData<?> sheetData : sheetDataList) {
                genSheetData(sheetData, workbook);
            }
            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 将单个 Sheet 的数据放入到 WorkBook 中
     *
     * @param sheetData Sheet 的数据对象
     * @param workbook  Excel 的数据载体对象
     * @throws IllegalAccessException 通过反射的方式设置对象属性时出现一场抛出
     */
    private void genSheetData(SheetData<?> sheetData, Workbook workbook)
            throws IllegalAccessException {
        List<?> data = sheetData.data();
        if (data == null || data.isEmpty()) return;

        Class<?> clazz = sheetData.dataClass();
        Sheet sheet = workbook.createSheet(sheetData.sheetName());
        List<Field> fields = getAllFields(clazz);
        fields.sort(ExcelTool::cellPropsCompare);
        // 生成头部信息（列名，头部单元格样式等）
        makeHeader(workbook, sheet, fields);

        for (int i = 0; i < data.size(); ++i) {
            Row row = sheet.createRow(i);
            fillRowStyle(workbook, clazz, row);
            Object t = data.get(i);
            for (int j = 0; j < fields.size(); j++) {
                Field field = fields.get(j);
                field.setAccessible(true);
                Object value = field.get(t);
                Cell cell = row.createCell(j, CellType.STRING);
                fillDataCellValue(cell, value, field.getType());
                fillDataCellStyle(workbook, field, cell);
            }
        }
    }

    private void fillDataCellValue(Cell cell, Object value, Class<?> type) {
        if (RichTextString.class.isAssignableFrom(type)) {
            cell.setCellValue((RichTextString) value);
            return;
        }
        if (Number.class.isAssignableFrom(type)) {
            cell.setCellValue(((Number) (value)).doubleValue());
            return;
        }
        if (Date.class.isAssignableFrom(type)) {
            cell.setCellValue((Date) value);
            return;
        }
        if (Calendar.class.isAssignableFrom(type)) {
            cell.setCellValue((Calendar) value);
            return;
        }
        if (LocalDate.class == type) {
            cell.setCellValue((LocalDate) value);
            return;
        }
        if (LocalDateTime.class == type) {
            cell.setCellValue((LocalDateTime) value);
            return;
        }
        cell.setCellValue(String.valueOf(value));
    }

    private void fillRowStyle(Workbook workbook, Class<?> clazz, Row row) {
        if (clazz == null || row == null) return;
        RowProps rowProps = clazz.getDeclaredAnnotation(RowProps.class);
        if (rowProps == null) return;
        CellStyle style = workbook.createCellStyle();
        if (!rowProps.rowStyle().isEmpty()) {
            style.cloneStyleFrom((CellStyle) context.getBean(rowProps.rowStyle()));
            row.setRowStyle(style);
            return;
        }
        style.cloneStyleFrom(context.getBean(rowProps.rowStyleClass()));
        row.setRowStyle(style);
    }

    /**
     * 填充数据单元格样式
     */
    private void fillDataCellStyle(Workbook workbook, Field field, Cell cell) {
        CellProps props = field.getDeclaredAnnotation(CellProps.class);
        if (props == null) return;
        CellStyle style = workbook.createCellStyle();
        if (!props.cellStyle().isEmpty()) {
            style.cloneStyleFrom((CellStyle) context.getBean(props.cellStyle()));
            cell.setCellStyle(style);
            return;
        }
        style.cloneStyleFrom(context.getBean(props.cellStyleClass()));
        cell.setCellStyle(style);
    }

    /**
     * 填充首行（列名）样式
     */
    private void fillHeaderCellStyle(Workbook workbook, Field field, Cell cell) {
        CellProps props = field.getDeclaredAnnotation(CellProps.class);
        if (props == null) return;
        CellStyle style = workbook.createCellStyle();
        if (!props.headerCellStyle().isEmpty()) {
            style.cloneStyleFrom((CellStyle) context.getBean(props.headerCellStyle()));
            cell.setCellStyle(style);
            return;
        }
        style.cloneStyleFrom(context.getBean(props.headerCellStyleClass()));
        cell.setCellStyle(style);
    }

    private static int cellPropsCompare(Field a, Field b) {
        CellProps cp1 = a.getDeclaredAnnotation(CellProps.class);
        CellProps cp2 = b.getDeclaredAnnotation(CellProps.class);
        if (cp1 == null && cp2 == null) return 0;
        if (cp1 == null || cp2 == null) {
            return cp1 == null ? 1 : -1;
        }
        return cp1.order() - cp2.order();
    }

    private void makeHeader(Workbook workbook, Sheet sheet, List<Field> fields) {
        Row header = sheet.createRow(0);
        for (int i = 0; i < fields.size(); i++) {
            Cell cell = header.createCell(i);
            Field field = fields.get(i);
            CellProps props = field.getDeclaredAnnotation(CellProps.class);
            cell.setCellValue(field.getName());
            fillHeaderCommon(cell, props);
            fillHeaderCellStyle(workbook, field, cell);
        }
    }

    private void fillHeaderCommon(Cell cell, CellProps props) {
        if (props == null) return;
        if (!props.colName().isEmpty()) {
            cell.setCellValue(props.colName());
        }
    }
}
