package org.xhliu.excel_handler.service;

import org.xhliu.excel_handler.entity.InsuredEntity;

import java.util.List;

/**
 * @author lxh
 */
public interface XssDataService {
    List<InsuredEntity> exampleData();
}
